const axios = require("axios");

module.exports = class JSONPlaceholderController {
  static async getUsers(req, res) {
    try {
      const response = await axios.get(
        "https://jsonplaceholder.typicode.com/users"
      );
      const users = response.data;
      res
        .status(200)
        .json({
          message:
            "Aqui estão os usuários captados da Api pública JSONPlaceholder",
          users,
        });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Falha ao encontrar usuários" });
    }
  }

  static async getUsersWebsiteIO(req,res){
    try{
      const response = await axios.get(
        "https://jsonplaceholder.typicode.com/users"
      );
      const users = response.data.filter(
        (user)=> user.website.endsWith(".io")
      )
      const Banana = users.length
      res.status(200).json({
          message:
            "AQUI ESTÃO OS USERS COM DOMINIO IO. Quantos bananas tem esse dominio:",Banana,
          users,
        });
    }
    catch(error){
      console.log(error)
      res.status(500).json({error:"Deu ruim",error})
    }
  }

  static async getUsersWebsiteCOM(req,res){
    try{
        const response = await axios.get(
          "https://jsonplaceholder.typicode.com/users"
        );
        const users = response.data.filter(
          (user)=> user.website.endsWith(".com")
        )
        const Banana = users.length
        res.status(200).json({
            message:
              "AQUI ESTÃO OS USERS COM DOMINIO COM. Quantos bananas tem esse dominio:",Banana,
            users,
          });
      }
      catch(error){
        console.log(error)
        res.status(500).json({error:"Deu ruim",error})
      }
    }
    
  static async getUsersWebsiteNET(req,res){
    try{
        const response = await axios.get(
          "https://jsonplaceholder.typicode.com/users"
        );
        const users = response.data.filter(
          (user)=> user.website.endsWith(".net")
        )
        const Banana = users.length
        res.status(200).json({
            message:
              "AQUI ESTÃO OS USERS COM DOMINIO NET. Quantos bananas tem esse dominio:",Banana,
            users,
          });
      }
      catch(error){
        console.log(error)
        res.status(500).json({error:"Deu ruim",error})
      }
    }

    static async getCountDomain(req, res) {
      const { dominio } = req.query;
    
      try {
        const response = await axios.get(
          "https://jsonplaceholder.typicode.com/users"
        );
        const users = response.data.filter((user) => user.website.endsWith(`${dominio}`));
        const Banana = users.length;
    
        res.status(200).json({
          message: `O número de usuários no domínio ${dominio.toUpperCase()} é ${Banana}`
        });
      } catch (error) {
        console.log(error);
        res.status(500).json({ error: "Deu ruim", error });
      }
    }    
  };