class TeacherController{

    static alunos = [];

    static async getAluno(req, res) {
        res.status(200).json({
            algumaMensagem:{

                algumaMensagem:'Euller',
                algumaNúmero:26,
            }
        });
    }

    //cadastrar aluno
    static async postAluno(req, res){
        const {nome, idade, profissao, cursoMatriculado} = req.body;
        const {_id} = req.params;

        const novoAluno = {
            nome,
            idade,
            profissao,
            cursoMatriculado,
            _id
        }

        TeacherController.alunos.push(novoAluno);
        console.log('aluno cadastrado', novoAluno);

        res.status(200).json({message: 'aluno cadastrado'})
    }

    static async updateAluno(req, res) {
        const { id, nome, idade, profissao, cursoMatriculado} = req.body;
        if( id !== "" && nome !== "" && idade !== null && profissao !== "" && cursoMatriculado !== ""){
            return res.status(200).json({ message: "Aluno Editado"});
        }
        
        else{
            return res.status(200).json({ message: "Não foi possível editar"});
        }
    }
        static async deleteAluno(req, res) {
            const { id } = req.query;
            const alunoIndex = TeacherController.alunos.findIndex(aluno => aluno._id === id);
            if (alunoIndex === -1) {
                return res.status(404).json({message: " Aluno não encontrado"});
            }
            TeacherController.alunos.splice(alunoIndex, 1);

         console.log("Aluno removido", id);
         res.status(200).json({message:"Aluno removido com sucesso"});
         }
         
}

module.exports = TeacherController